# PRA (Production Ready Application)

PRA is a string-boot application which follow as much as the production Ready application concern, i.e. it's act as a template for an Production Ready Application.
From local to fully managed by CI/CD.

##### Application Setup Requirements
  - Intellij IDEA
  - java(1.8 or +)
  - postgresql
  - Maven

##### 1. Clone the repository

On your local machine first clone the git repository and open it in intellij or
Directly from File->new->Project from version control->Git in intellij


```sh

$ git clone https://gitlab.com/pushonly14u/pra
                or
 https://gitlab.com/pushonly14u/pra in intellij

```

##### 3. intellij configuration

Once Project loaded open "Edit Configuration" -> "+" -> "Maven"
Put "Command Line" as = spring-boot:run

Then Click on "Runner" tab on same window -> "Environment Variables" -> Add following values.
Don't copy-paste directly.
```sh
ACTIVE_PROFILE = local
DATASOURCE_USERNAME = YourDBUserName
DATASOURCE_NAME	= YourDBName
DATASOURCE_PASSWORD	= YourDBPassword
```
##### 3. Run the application

Now all set, we only need to run the application by clicking the Run Button

##### Application is started you can look into browser at:
```sh
localhost:8081/api/
```
If you see 404 Not found then it's working.