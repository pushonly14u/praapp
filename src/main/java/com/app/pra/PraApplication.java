package com.app.pra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraApplication {

	public static void main(String[] args) {
		SpringApplication.run(PraApplication.class, args);
	}

}
