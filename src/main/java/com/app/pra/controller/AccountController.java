package com.app.pra.controller;

import com.app.pra.service.AccountService;
import com.app.pra.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public List<Account> getAll(){
        return accountService.getAll();
    }

    @GetMapping("/{id}")
    public Account get(@PathVariable int id){
        return accountService.get(id);
    }

    @PostMapping
    public Account add(@Valid @RequestBody Account account, int userId){
        return accountService.add(account, userId);
    }

}
