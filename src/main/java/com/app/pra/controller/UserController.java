package com.app.pra.controller;

import com.app.pra.model.User;
import com.app.pra.model.updateModel.UpdatedUser;
import com.app.pra.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User get(@PathVariable int id){
        return userService.get(id);
    }

    @PostMapping
    public User add(@Valid @RequestBody User user){
        return userService.add(user);
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @Valid @RequestBody UpdatedUser updatedUser){
        return userService.update(id, updatedUser);
    }
}
