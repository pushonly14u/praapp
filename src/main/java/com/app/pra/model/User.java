package com.app.pra.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@SequenceGenerator(name="user_seq", initialValue = 101)
@Table(name = "\"user\"")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @Column(length = 10, updatable = false)
    private int id;

    @NotNull(message = "\"Name\" must required")
    @NotEmpty(message = "\"Name\" must required")
    @Column(length = 50)
    private String name;

    @NotNull(message = "\"Email\" must required")
    @NotEmpty(message = "\"Email\" must required")
    @Column(length = 50)
    private String  email;

    @Column(name = "created_at")
    @CreationTimestamp
    private Timestamp createdAt;

    @Column(name = "last_updated")
    @UpdateTimestamp
    private Timestamp lastUpdated;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("user")
    public Set<Account> accounts = new HashSet<>();

}
