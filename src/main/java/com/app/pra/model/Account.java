package com.app.pra.model;

import com.app.pra.utility.Account_type;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Table(name = "account")
@SequenceGenerator(name="acc_seq", initialValue = 5001)
public class Account implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_seq")
    @Column(length = 10)
    public int account_no;

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    public Account_type account_type;

    @Column(length = 15)
    public long balance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties(value = {"accounts", "createdAt", "lastUpdated"})
    public User user;
}
