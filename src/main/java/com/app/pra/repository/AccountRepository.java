package com.app.pra.repository;

import com.app.pra.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findById(int id);
}
