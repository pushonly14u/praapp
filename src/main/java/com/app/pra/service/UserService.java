package com.app.pra.service;

import com.app.pra.model.User;
import com.app.pra.model.updateModel.UpdatedUser;

import java.util.List;

public interface UserService {
    List<User> getAll();
    User get(int id);
    User add(User user);
    User update(int userId, UpdatedUser updatedUser);
}
