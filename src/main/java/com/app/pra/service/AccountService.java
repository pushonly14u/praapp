package com.app.pra.service;

import com.app.pra.model.Account;
import java.util.List;

public interface AccountService {
    List<Account> getAll();
    Account get(int id);
    Account add(Account account, int userId);
}
