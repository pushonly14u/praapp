package com.app.pra.service.impl;

import com.app.pra.exception.ResourceNotFoundException;
import com.app.pra.model.User;
import com.app.pra.model.updateModel.UpdatedUser;
import com.app.pra.repository.UserRepository;
import com.app.pra.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User get(int id) {
        User user = userRepository.findById(id);
        if(user==null){
            throw  new ResourceNotFoundException("No User exist with ID "+id);
        }
        return user;
    }

    @Override
    public User add(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(int userId, UpdatedUser updatedUser) {
        User user = userRepository.findById(userId);
        if(user==null){
            throw  new ResourceNotFoundException("No User exist with ID "+userId);
        }
        if((updatedUser.getEmail())!=null){
            user.setEmail(updatedUser.getEmail());
        }
        if((updatedUser.getName())!=null){
            user.setName(updatedUser.getName());
        }
        return userRepository.save(user);
    }
}
