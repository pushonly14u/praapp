package com.app.pra.service.impl;


import com.app.pra.exception.ResourceNotFoundException;
import com.app.pra.model.Account;
import com.app.pra.model.User;
import com.app.pra.repository.AccountRepository;
import com.app.pra.repository.UserRepository;
import com.app.pra.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account get(int account_no) {
        Account account = accountRepository.findById(account_no);
        if(account == null){
            throw new ResourceNotFoundException("No account exist with Account_no "+account_no);
        }
        return accountRepository.findById(account_no);
    }

    @Override
    public Account add(@Valid @RequestBody Account account, @Valid @RequestParam int userId) {
        User user = userRepository.findById(userId);
        if(user == null){
            throw new ResourceNotFoundException("No User exist with ID "+userId);
        }
        account.setUser(user);
        return accountRepository.save(account);
    }
}
